;;; pretex.el --- preview LaTeX fragments everywhere  -*- lexical-binding: t; -*-
;; Copyright (C) 2022 Zaichuan Du
;;
;; Author:  Zaichuan Du <duzaichuan@hotmail.com>
;; Created: June 03, 2022
;; Modified: October 04, 2022
;; Version: 0.1.0
;; Keywords: tex,tools
;; Homepage: https://github.com/yangsheng6810/org-latex-instant-preview
;; Package-Requires: ((emacs "26") (s "1.8.0") (posframe "0.8.0") (org "9.3") (dash "2.17.0"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  This package provides instant preview of LaTeX snippets via MathJax outputed
;;  SVG.
;;
;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;; Code:
;;;

(require 'image-mode)
(require 's)
(require 'dash)
(require 'org)
(require 'posframe)
(require 'org-element)

(defcustom pretex-tex2svg-bin "~/node_modules/mathjax-node-cli/bin/tex2svg"
  "Location of tex2svg executable."
  :group 'pretex
  :type '(string))

(defcustom pretex-delay 0.1
  "Number of seconds to wait before a re-compilation."
  :group 'pretex
  :type '(number))

(defcustom pretex-scale 1.0
  "Scale of preview."
  :group 'pretex
  :type '(float))

(defcustom pretex-border-color "black"
  "Color of preview border."
  :group 'pretex
  :type '(color))

(defcustom pretex-border-width 1
  "Width of preview border."
  :group 'pretex
  :type '(integer))

(defcustom pretex-user-latex-definitions
  '("\\newcommand{\\ensuremath}[1]{#1}"
    "\\renewcommand{\\usepackage}[2][]{}")
  "Custom LaTeX definitions used in preview.

\\usepackage redefined since MathJax does not support it"
  :group 'pretex
  :type '(repeat string))

(defcustom pretex-posframe-position-handler
  #'pretex-poshandler
  "The handler for posframe position."
  :group 'pretex
  :type '(function))

(defcustom pretex-posframe-position
  'tex-end
  "The position that will be used by posframe handler."
  :group 'pretex
  :type '(choice (const :tag "Current Point" point)
                 (const :tag "End of TeX String" tex-end)))

(defcustom pretex-inhibit-envs
  '("tikzpicture")
  "List of environments that shall not preview"
  :group 'pretex
  :type '(repeat string))

(defcustom pretex-inhibit-commands
  '("\\tikz")
  "List of commands that shall not preview"
  :group 'pretex
  :type '(repeat string))

(defconst pretex--output-buffer-prefix "*pretex*"
  "Prefix for buffer to hold the output.")

(defconst pretex--posframe-buffer "*pretex*"
  "Buffer to hold the preview.")

(defvar pretex-keymap
  (let ((map (make-sparse-keymap)))
    (define-key map "\C-g" #'pretex-abort-preview)
    map)
  "Keymap for reading input.")

(defvar pretex--process nil)
(defvar pretex--timer nil)
(defvar-local pretex--last-tex-string nil)
(defvar-local pretex--last-position nil)
(defvar-local pretex--position nil)
(defvar-local pretex--last-preview nil)
(defvar-local pretex--current-window nil)
(defvar-local pretex--output-buffer nil)
(defvar-local pretex--is-inline nil)
(defvar-local pretex--force-hidden nil)

(defun pretex-poshandler (info)
  "Default position handler for posframe.

Uses the end point of the current LaTeX fragment for inline math,
and centering right below the end point otherwise. Positions are
calculated from INFO."
  (if pretex--is-inline
      (posframe-poshandler-point-bottom-left-corner info)
    (if (fboundp 'posframe-poshandler-point-window-center)
        (posframe-poshandler-point-window-center info)
      (pretex--poshandler-point-window-center info))))

(defun pretex--poshandler-point-window-center (info)
  "Posframe's position handler.

Get a position which let posframe stay right below current
position, centered in the current window. The structure of INFO
can be found in docstring of `posframe-show'.

This function will be removed once a similar poshandler is
available in upstream."
  (let* ((window-left (plist-get info :parent-window-left))
         (window-width (plist-get info :parent-window-width))
         (posframe-width (plist-get info :posframe-width))
         (posframe-height (plist-get info :posframe-height))
         (y-pixel-offset (plist-get info :y-pixel-offset))
         (ymax (plist-get info :parent-frame-height))
         (window (plist-get info :parent-window))
         (position-info (plist-get info :position-info))
         (header-line-height (plist-get info :header-line-height))
         (tab-line-height (plist-get info :tab-line-height))
         (y-top (+ (cadr (window-pixel-edges window))
                   tab-line-height
                   header-line-height
                   (- (or (cdr (posn-x-y position-info)) 0)
                      ;; Fix the conflict with flycheck
                      ;; https://lists.gnu.org/archive/html/emacs-devel/2018-01/msg00537.html
                      (or (cdr (posn-object-x-y position-info)) 0))
                   y-pixel-offset))
         (font-height (plist-get info :font-height))
         (y-bottom (+ y-top font-height)))
    (cons (+ window-left (/ (- window-width posframe-width) 2))
          (max 0 (if (> (+ y-bottom (or posframe-height 0)) ymax)
                     (- y-top (or posframe-height 0))
                   y-bottom)))))

(defun pretex--clean-up ()
  "Clean up timer, process, and variables."
  (pretex--hide)
  (when pretex--process
    (kill-process pretex--process))
  (when (get-buffer pretex--output-buffer)
    (let ((kill-buffer-query-functions nil))
      (kill-buffer pretex--output-buffer)))
  (setq pretex--process nil
        pretex--last-tex-string nil
        pretex--last-position nil
        pretex--current-window nil))

;;;###autoload
(defun pretex-stop ()
  "Stop instant preview of LaTeX snippets."
  (interactive)
  ;; only needed for manual start/stop
  (remove-hook 'after-change-functions #'pretex--prepare-timer t)
  (pretex--hide)
  (pretex--interrupt-rendering))

(defun pretex--prepare-timer (&rest _)
  "Prepare timer to call re-compilation."
  (when pretex--timer
    (cancel-timer pretex--timer)
    (setq pretex--timer nil))
  (if (and (or (derived-mode-p 'org-mode)
               (derived-mode-p 'latex-mode)
               (derived-mode-p 'markdown-mode)
	       (derived-mode-p 'eww-mode))
           (pretex--in-latex-p))
      (setq pretex--timer
            (run-with-idle-timer pretex-delay nil #'pretex-start))
    (pretex--hide)))

(defun pretex--remove-math-delimeter (ss)
  "Chop LaTeX delimeters from SS."
  (unless pretex--is-inline
    (setq pretex--is-inline
          (or (s-starts-with? "\\(" ss)
              (s-starts-with? "$" ss))))
  (s-with ss
    (s-chop-prefixes '("$$" "\\(" "$" "\\["))
    (s-chop-suffixes '("$$" "\\)" "$" "\\]"))))

(defun pretex--add-color (ss)
  "Wrap SS with color from default face."
  (let ((color (face-foreground 'default)))
    (format "\\color{%s}{%s}" color ss)))

(defun pretex--equal-or-member (elm target)
  (if (listp target)
      (member elm target)
    (equal elm target)))

(defun pretex--in-latex-p ()
  "Return t if current point is in a LaTeX fragment, nil otherwise."
  (cond ((derived-mode-p 'org-mode)
         (let ((datum (org-element-context)))
           (or (memq (org-element-type datum) '(latex-environment latex-fragment))
               (and (memq (org-element-type datum) '(export-block))
                    (equal (org-element-property :type datum) "LATEX")))))
        ((derived-mode-p 'latex-mode)
         (pretex--tex-in-latex-p))
        ((derived-mode-p 'markdown-mode)
         (pretex--equal-or-member 'markdown-math-face (get-text-property (point) 'face)))
	((derived-mode-p 'eww-mode)
	 (org-inside-LaTeX-fragment-p))
        (t (message "We only support org-mode, latex-mode, and markdown-mode")
           nil)))

(defun pretex--tex-in-latex-p ()
  "Return t if in LaTeX fragment in `latex-mode', nil otherwise."
  (let ((faces (face-at-point nil t)))
    (or (-contains? faces 'font-latex-math-face)
        (-contains? faces 'preview-face))))

(defun pretex--in-eww-p ()
  "Test if point is inside a LaTeX fragment.
I.e. after a \\begin, \\(, \\[, $, or $$, without the corresponding closing
sequence appearing also before point.
Even though the matchers for math are configurable, this function assumes
that \\begin, \\(, \\[, and $$ are always used.  Only the single dollar
delimiters are skipped when they have been removed by customization.
The return value is nil, or a cons cell with the delimiter and the
position of this delimiter.

This function does a reasonably good job, but can locally be fooled by
for example currency specifications.  For example it will assume being in
inline math after \"$22.34\".  The LaTeX fragment formatter will only format
fragments that are properly closed, but during editing, we have to live
with the uncertainty caused by missing closing delimiters.  This function
looks only before point, not after."
  (catch 'exit
    (let ((pos (point))
	  (dodollar (member "$" (plist-get org-format-latex-options :matchers)))
	  (lim (progn
		 (re-search-backward (concat "^\\(" paragraph-start "\\)") nil
				     'move)
		 (point)))
	  dd-on str (start 0) m re)
      (goto-char pos)
      (when dodollar
	(setq str (concat (buffer-substring lim (point)) "\000 X$.")
	      re (nth 1 (assoc "$" org-latex-regexps)))
	(while (string-match re str start)
	  (cond
	   ((= (match-end 0) (length str))
	    (throw 'exit (cons "$" (+ lim (match-beginning 0) 1))))
	   ((= (match-end 0) (- (length str) 5))
	    (throw 'exit nil))
	   (t (setq start (match-end 0))))))
      (when (setq m (re-search-backward "\\(\\\\begin{[^}]*}\\|\\\\(\\|\\\\\\[\\)\\|\\(\\\\end{[^}]*}\\|\\\\)\\|\\\\\\]\\)\\|\\(\\$\\$\\)" lim t))
	(goto-char pos)
	(and (match-beginning 1) (throw 'exit (cons (match-string 1) m)))
	(and (match-beginning 2) (throw 'exit nil))
	;; count $$
	(while (re-search-backward "\\$\\$" lim t)
	  (setq dd-on (not dd-on)))
	(goto-char pos)
	(when dd-on (cons "$$" m))))))

(defun pretex--has-latex-overlay ()
  "Return t if there is LaTeX overlay showing."
  (--first (or (overlay-get it 'xenops-overlay-type)
               (equal 'org-latex-overlay (overlay-get it 'org-overlay-type)))
           (append (overlays-at (point)) (overlays-at (1- (point))))))

(defun pretex--get-tex-string ()
  "Return the string of LaTeX fragment."
  (cond ((derived-mode-p 'org-mode)
         (let ((datum (org-element-context)))
           (org-element-property :value datum)))
        ((derived-mode-p 'latex-mode)
         (let (begin end)
           (save-excursion
             (while (pretex--tex-in-latex-p)
               (backward-char))
             (setq begin (1+ (point))))
           (save-excursion
             (while (pretex--tex-in-latex-p)
               (forward-char))
             (setq end (point)))
           (let ((ss (buffer-substring-no-properties begin end)))
             (message "ss is %S" ss)
             ss)))
	((derived-mode-p 'eww-mode)
         (let (begin end)
           (save-excursion
             (while (pretex--in-latex-p)
               (backward-char))
             (setq begin (1+ (point))))
           (save-excursion
             (while (pretex--in-latex-p)
               (forward-char))
             (setq end (point)))
           (let ((ss (buffer-substring-no-properties begin end)))
             ;; (message "ss is %S" ss)
             ss)))
        ((derived-mode-p 'markdown-mode)
         (let (begin end)
           (save-excursion
             (setq begin (prop-match-beginning
                          (text-property--find-end-backward
                           (point) 'face 'markdown-math-face #'yang/equal-or-member))))
           (save-excursion
             (setq end (prop-match-end
                        (text-property--find-end-forward
                         (point) 'face 'markdown-math-face #'yang/equal-or-member)))
             (unless (looking-at (rx (or "$$" "\\]")))
               (setq pretex--is-inline t)
               (message "setting is-line to %s" pretex--is-inline)))
           (let ((ss (buffer-substring-no-properties begin end)))
             (message "ss is %S" ss)
             ss)))
        (t "")))

(defun pretex--get-tex-position ()
  "Return the end position of LaTeX fragment."
  (cond ((eq pretex-posframe-position 'tex-end)
         (cond ((derived-mode-p 'org-mode)
                (let ((datum (org-element-context)))
                  (org-element-property :end datum)))
               ((derived-mode-p 'latex-mode)
                (save-excursion
                  (while (pretex--tex-in-latex-p)
                    (forward-char))
                  (point)))
	       ((derived-mode-p 'eww-mode)
                (save-excursion
                  (while (pretex--in-latex-p)
                    (forward-char))
                  (point)))
               ((derived-mode-p 'markdown-mode)
                (save-excursion
                  (prop-match-end
                   (text-property--find-end-forward
                    (point) 'face 'markdown-math-face #'yang/equal-or-member))))
               (t (message "Only org-mode, latex-mode, and markdown-mode supported") nil)))
        ((eq pretex-posframe-position 'point)
         (point))
        (t (message "pretex-posframe-position set incorrectly"))))

(defun pretex--need-remove-delimeters ()
  "Return t if need to remove delimeters."
  (cond ((derived-mode-p 'org-mode)
         (let ((datum (org-element-context)))
           (memq (org-element-type datum) '(latex-fragment))))
        ((derived-mode-p 'latex-mode)
         ;; (message "Not implemented.")
         t)
        ((derived-mode-p 'markdown-mode)
         ;; (message "Not implemented.")
         t)
        (t "")))

(defun pretex--get-headers ()
  "Return a string of headers."
  (cond ((derived-mode-p 'org-mode)
         (plist-get (org-export-get-environment
                     (org-export-get-backend 'latex))
                    :latex-header))
        ((derived-mode-p 'latex-mode)
         ;; (message "Get header not supported in latex-mode yet.")
         "")
        ((derived-mode-p 'markdown-mode)
         ;; (message "Get header not supported in markdown-mode yet.")
         "")
        (t "")))

(defun pretex-inhibit (tex-string)
  "Returns t if we do not want TEX-STRING to preview."
  ;; checks for tikz
  (or (s-matches-p
       (rx-to-string
        `(: "\\begin{" (or ,@pretex-inhibit-envs)))
       tex-string)
      (s-matches-p
       (rx-to-string
        `(or ,@pretex-inhibit-commands))
       tex-string)))

;;;###autoload
(defun pretex-start (&rest _)
  "Start instant preview."
  (interactive)
  (unless (and (not (string= pretex-tex2svg-bin ""))
               (executable-find pretex-tex2svg-bin))
    (message "You need to set pretex-tex2svg-bin
for instant preview to work!")
    (error "Variable pretex-tex2svg-bin is not set correctly"))

  ;; Only used for manual start
  (when (equal this-command #'pretex-start)
    (add-hook 'after-change-functions #'pretex--prepare-timer nil t))

  (if (and (or (derived-mode-p 'org-mode)
               (derived-mode-p 'latex-mode)
               (derived-mode-p 'markdown-mode)
	       (derived-mode-p 'eww-mode))
       (pretex--in-latex-p)
       (not (pretex--has-latex-overlay)))
      (let ((--dummy-- (setq pretex--is-inline nil))
            (tex-string (pretex--get-tex-string))
            (latex-header
             (concat (s-join "\n" pretex-user-latex-definitions)
                     "\n"
                     (pretex--get-headers))))
        (unless (pretex-inhibit tex-string)
          (setq pretex--current-window (selected-window))
          ;; the tex string from latex-fragment includes math delimeters like
          ;; $, $$, \(\), \[\], and we need to remove them.
          (when (pretex--need-remove-delimeters)
            (setq tex-string (pretex--remove-math-delimeter tex-string)))

          (setq pretex--position (pretex--get-tex-position)
                ;; set forground color for LaTeX equations.
                tex-string (concat latex-header
                                   (pretex--add-color tex-string)))
          (if (and pretex--last-tex-string
                   (equal tex-string
                          pretex--last-tex-string))
              ;; TeX string is the same, we only need to update posframe
              ;; position.
              (when (and pretex--last-position
                         (equal pretex--position pretex--last-position)
                         ;; do not force showing posframe when a render
                         ;; process is running.
                         (not pretex--process)
                         (not pretex--force-hidden))
                (pretex--show))
            ;; reset `-force-hidden'
            (setq pretex--force-hidden nil)
            ;; A new rendering is needed.
            (pretex--interrupt-rendering)
            (pretex--render tex-string))))
    ;; Hide posframe when not on LaTeX
    (pretex--hide)))

(defun pretex--interrupt-rendering ()
  "Interrupt current running rendering."
  (when pretex--process
    (condition-case nil
        (kill-process pretex--process)
      (error "Faild to kill process"))
    (setq pretex--process nil
          ;; last render for tex string is invalid, therefore need to invalid
          ;; its cache
          pretex--last-tex-string nil
          pretex--last-preview nil))
  (when (get-buffer pretex--output-buffer)
    (let ((kill-buffer-query-functions nil))
      (kill-buffer pretex--output-buffer))))

(defun pretex--render (tex-string)
  "Render TEX-STRING to buffer, async version.

Showing at point END"
  (let (message-log-max)
    (message "Instant LaTeX rendering"))
  (pretex--interrupt-rendering)
  (setq pretex--last-tex-string tex-string)
  (setq pretex--last-position pretex--position)
  (get-buffer-create pretex--output-buffer)

  (setq pretex--process
        (make-process
         :name "pretex"
         :buffer pretex--output-buffer
         :command (append (list pretex-tex2svg-bin
                                tex-string)
                          (when pretex--is-inline
                            '("--inline")))
         ;; :stderr ::my-err-buffer
         :sentinel
         (lambda (&rest _)
           (condition-case nil
               (progn
                 (pretex--fill-posframe-buffer)
                 (pretex--show)
                 (when (get-buffer pretex--output-buffer)
                   (let ((kill-buffer-query-functions nil))
                     (kill-buffer pretex--output-buffer))))
             (error nil))
           ;; ensure -process is reset
           (setq pretex--process nil)))))

(defun pretex--insert-into-posframe-buffer (ss)
  "Insert SS into posframe buffer."
  (buffer-disable-undo pretex--posframe-buffer)
  (let ((inhibit-message t)
        (message-log-max nil))
    (with-current-buffer pretex--posframe-buffer
      (image-mode-as-text)
      (erase-buffer)
      (insert ss)
      (image-mode))))

(defun pretex--fill-posframe-buffer ()
  "Write SVG in posframe buffer."
  (let ((ss (with-current-buffer pretex--output-buffer
              (buffer-string))))
    (unless (get-buffer pretex--posframe-buffer)
      (get-buffer-create pretex--posframe-buffer))
    ;; when compile error, ss is exactly the error message, so we do nothing.
    ;; Otherwise when compile succeed and need scaling, do some hacks
    (when (and (s-contains-p "svg" ss)
               (not (equal pretex-scale 1.0)))
      (setq ss
            (concat
             ;; 100% seems wierd
             "<svg height=\"110%\">"
             ;; ad-hoc for scaling
             (format "<g transform=\"scale(%s)\">" pretex-scale)
             ss
             "</g></svg>")))
    (pretex--insert-into-posframe-buffer ss)
    (setq pretex--last-preview ss)))

(defun pretex--show (&optional display-point)
  "Show preview posframe at DISPLAY-POINT."
  (unless display-point
    (setq display-point pretex--position))
  (when (and pretex--current-window
             (posframe-workable-p)
             (<= (window-start) display-point (window-end))
             (not pretex--force-hidden))
    (unless (get-buffer pretex--posframe-buffer)
      (get-buffer-create pretex--posframe-buffer)
      (when (and pretex--last-preview
                 (not (string= "" pretex--last-preview)))
        ;; use cached preview
        (pretex--insert-into-posframe-buffer pretex--last-preview)))
    (let ((temp pretex--is-inline))
      (with-current-buffer pretex--posframe-buffer
        (setq pretex--is-inline temp)))

    ;; handle C-g
    (define-key pretex-keymap (kbd "C-g") #'pretex-abort-preview)
    (posframe-show pretex--posframe-buffer
                   :position display-point
                   :poshandler pretex-posframe-position-handler
                   :parent-window pretex--current-window
                   :internal-border-width pretex-border-width
                   :internal-border-color pretex-border-color
                   :hidehandler #'posframe-hidehandler-when-buffer-switch)))

(defun pretex--hide ()
  "Hide preview posframe."
  (define-key pretex-keymap (kbd "C-g") nil)
  (posframe-hide pretex--posframe-buffer)
  (when (get-buffer pretex--posframe-buffer)
    (setq pretex--last-preview
          (with-current-buffer pretex--posframe-buffer
            (let ((inhibit-message t)
                  (message-log-max nil))
              (image-mode-as-text)
              (buffer-substring-no-properties (point-min) (point-max)))))
    (kill-buffer pretex--posframe-buffer)))

(defun pretex-abort-preview ()
  "Abort preview."
  (interactive)
  (pretex--interrupt-rendering)
  (define-key pretex-keymap (kbd "C-g") nil)
  (setq pretex--force-hidden t)
  (pretex--hide))

;;;###autoload
(define-minor-mode pretex-mode
  "Instant preview of LaTeX in org-mode"
  :global nil :init-value nil :keymap pretex-keymap
  (if pretex-mode
      (progn
        (setq pretex--output-buffer
              (concat pretex--output-buffer-prefix (buffer-name)))
        (add-hook 'post-command-hook #'pretex--prepare-timer nil t))
    (remove-hook 'post-command-hook #'pretex--prepare-timer t)
    (pretex-stop)))

(provide 'pretex)
;;; pretex.el ends here
